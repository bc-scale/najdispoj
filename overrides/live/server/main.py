"""
Default main - do not modify

Put the modified file into /overrides/<override>/server/
"""
from fastapi import FastAPI

from apscheduler.schedulers.background import BackgroundScheduler

from server.services.geodata_service import GeodataService
from server.services.geocoding_service import GeocodingService
from server.services.routing_service import RoutingService
from server.services.static_data_service import StaticDataService
from server.services.dynamic_data_service import DynamicDataService
from server.services.api_service import APIService

from server.providers.geodata.overpass_api import OverpassAPIProvider
from server.providers.geodata.geofabrik import GeofabrikProvider

from server.providers.routing.opentripplanner1_5_0 import (
    OpenTripPlanner1_5_0Provider,
)

from server.providers.geocoding.photon import PhotonProvider

from server.providers.agencies.dpmo.static_data import DPMOStaticDataProvider
from server.providers.agencies.dpb.static_data import DPBStaticDataProvider
from server.providers.agencies.dpb.dynamic_data import DPBDynamicDataProvider
from server.providers.agencies.zsr.static_data import ZSRStaticDataProvider
from server.providers.agencies.idsjmk.static_data import (
    IDSJMKStaticDataProvider,
)

from server.core.coords import Coords
from server.core.bounding_box import BoundingBox
from server.core.osmfile import OSMFile
from server.core.gtfsfolder import GTFSFolder

from containers.nginx.manager import NginxManager
from containers.opentripplanner1_5_0.manager import OpenTripPlanner1_5_0Manager

import logging
import server.config as config

from server.credentials import dpb_host, dpb_login, dpb_pass

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s.%(msecs)03d - %(levelname)s - %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
)

"""Geodata service setup"""
geodata_service = GeodataService()
geodata_service.register_provider(
    "OverpassAPI", OverpassAPIProvider("https://overpass-api.de/api")
)
geodata_service.register_provider("Geofabrik", GeofabrikProvider())

"""Geocoding service setup"""
geocoding_service = GeocodingService()
geocoding_service.register_provider(
    "Photon", PhotonProvider("https://photon.komoot.io")
)

"""Static data service setup"""
static_data_service = StaticDataService(geodata_service)
static_data_service.register_provider("DPMO", DPMOStaticDataProvider())
static_data_service.register_provider("ZSR", ZSRStaticDataProvider())
static_data_service.register_provider(
    "DPB", DPBStaticDataProvider(dpb_host, dpb_login, dpb_pass)
)
static_data_service.register_provider("IDSJMK", IDSJMKStaticDataProvider())

"""Routing service setup"""
otp_manager = OpenTripPlanner1_5_0Manager("/mnt/data/otp150", 25515, xmx="3G")
routing_service = RoutingService(static_data_service, geodata_service)
routing_service.register_provider(
    "OpenTripPlanner 1.5.0",
    OpenTripPlanner1_5_0Provider("http://opentripplanner150:25515"),
)

"""Dynamic data service setup"""
dynamic_data_service = DynamicDataService()
dynamic_data_service.register_provider(
    "DPB",
    DPBDynamicDataProvider("0.0.0.0", config.network["dynamic_data_port"]),
)

"""API service setup"""
api_service = APIService(
    geocoding_service, routing_service, dynamic_data_service
)

"""Nginx setup"""
nginx_manager = NginxManager(
    config.network["host"],
    config.network["port"],
    config.network["najdispoj_port"],
    dynamic_data_port=config.network["dynamic_data_port"],
)

"""FastAPI & API endpoints setup"""
# TODO move to a separate file (to APIService)
app = FastAPI()


def update_osm():
    slovakia_pbf = OSMFile("/mnt/data/osm/slovakia.pbf")
    czech_republic_pbf = OSMFile("/mnt/data/osm/czech_republic.pbf")
    olomouc_osm = OSMFile("/mnt/data/osm/olomouc.osm")
    bratislava_osm = OSMFile("/mnt/data/osm/bratislava.osm")

    # Slovakia
    slovakia_pbf = geodata_service.load_data(
        "Geofabrik",
        slovakia_pbf.path,
        url="https://download.geofabrik.de/europe/slovakia-latest.osm.pbf",
    )
    if not slovakia_pbf:
        logging.error("Failed to download OSM data for Slovakia.")
        return

    # Czech Republic
    czech_republic_pbf = geodata_service.load_data(
        "Geofabrik",
        czech_republic_pbf.path,
        url="https://download.geofabrik.de/europe/czech-republic-latest.osm.pbf",
    )
    if not czech_republic_pbf:
        logging.error("Failed to download OSM data for Czech Republic.")
        return

    # Olomouc
    olomouc_osm = geodata_service.load_data(
        "OverpassAPI",
        olomouc_osm.path,
        bbox=BoundingBox(Coords(49.5277, 17.1665), Coords(49.6590, 17.3799)),
    )
    if not olomouc_osm:
        logging.error("Failed to download OSM data for Olomouc.")
        return

    # Bratislava
    bratislava_osm = geodata_service.load_data(
        "OverpassAPI",
        bratislava_osm.path,
        bbox=BoundingBox(Coords(47.9721, 16.8665), Coords(48.3263, 17.3557)),
    )
    if not bratislava_osm:
        logging.error("Failed to download OSM data for Bratislava.")
        return

    # # Merge OSM files
    OSMFile.merge(
        [olomouc_osm, bratislava_osm, slovakia_pbf, czech_republic_pbf],
        merged_osm.path,
    )
    merged_osm.cat(merged_pbf.path)


def update_gtfs():
    dpmo_static_data = GTFSFolder("/mnt/data/gtfs/dpmo/").set_label("DPMO")
    dpb_static_data = GTFSFolder("/mnt/data/gtfs/dpb/").set_label("DPB")
    zsr_static_data = GTFSFolder("/mnt/data/gtfs/zsr/").set_label("ZSR")
    idsjmk_static_data = GTFSFolder("/mnt/data/gtfs/idsjmk/").set_label(
        "IDSJMK"
    )

    # Olomouc
    dpmo_static_data = static_data_service.load_data(
        "DPMO", dpmo_static_data.path
    )
    if not dpmo_static_data:
        logging.error("Failed to download GTFS data.")
        return
    dpmo_static_data.enhance(merged_osm)

    # Bratislava
    dpb_static_data = static_data_service.load_data("DPB", dpb_static_data.path)
    if not dpb_static_data:
        logging.error("Failed to download GTFS data.")
        return
    (
        dpb_static_data.change_route_types("11", "3")
        .enhance(merged_osm)
        .revert_routes()
        .change_route_types("11", "800")
    )

    # ŽSR
    zsr_static_data = static_data_service.load_data("ZSR", zsr_static_data.path)
    if not zsr_static_data:
        logging.error("Failed to download GTFS data.")
        return
    zsr_static_data.enhance(merged_osm)

    # # IDSJMK
    idsjmk_static_data = static_data_service.load_data(
        "IDSJMK", idsjmk_static_data.path
    )
    if not idsjmk_static_data:
        logging.error("Failed to download GTFS data.")
        return
    (
        idsjmk_static_data.change_route_types("11", "3")
        .enhance(merged_osm)
        .revert_routes()
        .change_route_types("11", "800")
    )

    # Link OSM and build graph
    otp_manager.link_osm(merged_pbf.path)
    otp_manager.build_graph(
        [dpmo_static_data, dpb_static_data, zsr_static_data, idsjmk_static_data]
    )


# Config variables
merged_osm = OSMFile("/mnt/data/osm/merged.osm")
merged_pbf = OSMFile("/mnt/data/osm/merged.pbf")


@app.on_event("startup")
def init_app():
    # # Add scheduled jobs
    # scheduler = BackgroundScheduler()
    # scheduler.add_job(update_gtfs, "cron", hour="1")  # Runs at 1:00 (1 AM)
    # scheduler.start()

    # # Data initialization
    # update_osm()
    # update_gtfs()
    nginx_manager.build_client()
    otp_manager.serve(restart=True)


@app.get("/api/reverse")
async def reverse(coords: str):
    return api_service.reverse_geocode(coords)


@app.get("/api/autocomplete")
async def autocomplete(query: str):
    return api_service.autocomplete(query)


@app.get("/api/plan")
async def plan(
    origin: str,
    destination: str,
    arriveBy: bool,
    date: str,
    time: str,
    maxTransfers: int,
    maxWalkDistance: int,
    walkSpeed: int,
    optimize: str,
):
    # TODO make parameters lowercase (must be changed on client too)
    return api_service.get_plan(
        origin,
        destination,
        arriveBy,
        date,
        time,
        maxTransfers,
        maxWalkDistance,
        walkSpeed,
        optimize,
    )


@app.get("/api/vehicles")
async def vehicles():
    return api_service.get_vehicles()
