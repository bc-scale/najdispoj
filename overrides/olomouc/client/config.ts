// Olomouc
export default {
    title: 'MHD Olomouc · Najdispoj',
    ticket: false,
    api: {
        host: 'https://live.najdispoj.sk/api'
    },
    locales: {
        default: 'cs',
        fallback: 'en',
        supported: ['en', 'cs']
    },
    map: {
        vehicles: false,
        center: {
            lat: 49.5935,
            lng: 17.2569
        },
        zoom: 14,
    }
}