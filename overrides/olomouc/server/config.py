"""
Provider-independent variables are set in this file.
Provider-dependent variables should be set in the provider's
constructor (in main.py).
"""

# Olomouc
network = {
    "host": "live.najdispoj.sk",
    "port": 443,  # 443 for https, 80 for http
    "najdispoj_port": 25516,
}

area = {
    "bbox": (
        {"lat": 49.5277, "lng": 17.1665},
        {"lat": 49.6590, "lng": 17.3799},
    ),
    "center": {"lat": 49.5935, "lng": 17.2569},
}

osm = {
    "path_raw": "/mnt/data/osm/raw.osm",
    "path_altered": "/mnt/data/osm/altered.osm",
    "path_filtered": "/mnt/data/osm/filtered.pbf",
}

gtfs = {
    "path_raw": "/mnt/data/gtfs/raw/",
    "path_main": "/mnt/data/gtfs/main/",
    "path_zip": "/mnt/data/gtfs/gtfs.zip",
}

pfaedle = {
    "path": "/usr/src/app/vendor/pfaedle/build/pfaedle",
}
