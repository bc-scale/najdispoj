from fastapi import FastAPI

from apscheduler.schedulers.background import BackgroundScheduler

from server.providers.dpmo import DPMOStaticDataProvider

from server.services.geodata_service import GeodataService
from server.services.geocoding_service import GeocodingService
from server.services.routing_service import RoutingService
from server.services.static_data_service import StaticDataService
from server.services.api_service import APIService

from server.providers.photon import PhotonProvider
from server.providers.overpass_api import OverpassAPIProvider
from server.providers.opentripplanner1_5_0 import OpenTripPlanner1_5_0Provider

from containers.nginx.manager import NginxManager
from containers.opentripplanner1_5_0.manager import OpenTripPlanner1_5_0Manager

import logging
import server.config as config

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s - %(levelname)s - %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
)

"""Geodata service setup"""
geodata_service = GeodataService()
geodata_service.register_provider(
    "OverpassAPI", OverpassAPIProvider("https://overpass-api.de/api")
)
geodata_service.download_data("OverpassAPI")
geodata_service.filter_data()

"""Geocoding service setup"""
geocoding_service = GeocodingService()
geocoding_service.register_provider(
    "Photon", PhotonProvider("https://photon.komoot.io")
)

"""Static data service setup"""
static_data_service = StaticDataService(geodata_service)
static_data_service.register_provider("DPMO", DPMOStaticDataProvider())
static_data_service.download_data("DPMO")

"""Routing service setup"""
otp_manager = OpenTripPlanner1_5_0Manager("/mnt/data/otp150", 25515)
otp_manager.build_graph()
otp_manager.serve()
routing_service = RoutingService(static_data_service, geodata_service)
routing_service.register_provider(
    "OpenTripPlanner 1.5.0",
    OpenTripPlanner1_5_0Provider("http://opentripplanner150:25515"),
)

"""API service setup"""
api_service = APIService(geocoding_service, routing_service)

"""Nginx setup"""
nginx_manager = NginxManager(
    config.network["host"],
    config.network["port"],
    config.network["najdispoj_port"],
)
nginx_manager.build_client()

"""FastAPI & API endpoints setup"""
# TODO move to a separate file (to APIService)
app = FastAPI()


@app.on_event("startup")
def init_scheduler():
    def nightly():
        static_data_service.download_data("DPMO")
        otp_manager.build_graph()
        otp_manager.serve()

    scheduler = BackgroundScheduler()
    scheduler.add_job(nightly, "cron", hour="1")  # Runs at 1:00 (1 AM)
    scheduler.start()


@app.get("/api/reverse")
async def reverse(coords: str):
    return api_service.reverse_geocode(coords)


@app.get("/api/autocomplete")
async def autocomplete(query: str):
    return api_service.autocomplete(query)


@app.get("/api/plan")
async def plan(
    origin: str,
    destination: str,
    arriveBy: bool,
    date: str,
    time: str,
    maxTransfers: int,
    maxWalkDistance: int,
    walkSpeed: int,
    optimize: str,
):
    # TODO make parameters lowercase (must be changed on client too)
    return api_service.get_plan(
        origin,
        destination,
        arriveBy,
        date,
        time,
        maxTransfers,
        maxWalkDistance,
        walkSpeed,
        optimize,
    )


@app.get("/api/vehicles")
async def vehicles():
    return api_service.get_vehicles()
