# 🚍 **Najdispoj**

*The complete open source solution for creating and deploying your custom trip planner.*

*Najdispoj* is a *Docker*-based platform for creating and managing a custom door-to-door trip planner. It hides away the complexity of setting up *Docker* containers and lets people without deep knowledge of software engineering set up their own trip planner within a few days, if not hours.

## Main features:

- Mobile-first, fully responsive modern design.
- Multiple ways of entering the origin and destination points:
    - Map selection 🗺️
    - Address, place name (geocoding)
    - Geolocation 📡
    - Voice input 🗣️
- Real-time data support (alpha) 
- Extendable support for various providers of data, with the possibility of combining results from multiple sources.
- Modularity, extendability and ease of deployment and development.
- Full localization support.

Who is *Najdispoj* for?
- **Transit providers** - Make your services more accessible and attractive to potential passengers.
- **Public transport enthusiasts** - If your city/region lacks good trip-planning services, you can easily create one yourself using publicly available data.

The main goals of *Najdispoj* are:

🔴 Making **public transport** as **accessible and attractive** as possible.

🟡 Keeping it **easy to modify, extend and integrate** into other systems.

🔵 Promoting the use of **open source software** and **open data** in the public sphere.

## Planned features
- Dark mode 🌙
- Support for lesser-known country-specific data formats
- Admin dashboard
- Improved accessibility
- Progressive Web App support
- Integration of ticket payment systems
- ...

## Installation guide

1.  **Get a VPS/dedicated server**
    - At least **40 GB disk space**
        - Depending on the size of your region, more space might be necessary.
    - At least **4 GB RAM**
    - *Debian 10/11* recommended

2.  **Connect to the server via SSH as root**

3.  **Create user *najdispoj***

        adduser najdispoj

    - Follow the on-screen instructions.

4.  **Install *sudo***
    - [Instructions](https://unix.stackexchange.com/questions/354928/bash-sudo-command-not-found)
    - Commands:

            apt-get install sudo -y
            usermod -aG sudo najdispoj
    
    - > **(Optional)** Disable root login via SSH

5.  **Log in via SSH as user *najdispoj***

6.  **Install *Docker***
    - [Instructions part 1](https://docs.docker.com/engine/install/debian/)
    - [Instructions part 2](https://docs.docker.com/engine/install/linux-postinstall/)

7.  **Download and configure *Najdispoj***
    - Clone from this *Git* repository, or upload manually via FTP/SFTP to folder `/home/najdispoj/najdispoj`
    - Create a new folder inside /overrides (e.g. `/overrides/customconfig`) and copy the contents of `/overrides/bratislava` or `/overrides/olomouc` inside
    - Configure server
        - Edit files:

            `/overrides/customconfig/server/main.py`
            `/overrides/customconfig/server/config.py`
    - Configure client
        - Edit file:
            
            `/overrides/customconfig/client/config.ts` 
    - **Detailed instructions coming soon**

8.  **Get an SSL certificate**

        sudo apt-get install certbot -y
        sudo apt-get install ufw -y
        sudo ufw allow 80/tcp
        sudo certbot certonly --standalone <parameters>

    - Parameters:
        - Get a certificate without entering email:
            - *--register-unsafely-without-email*
        - Get a test certificate (e.g. when deploying for the first time):
            - *--staging*
        - [Advanced settings](https://eff-certbot.readthedocs.io/en/stable/using.html#certbot-commands)
    - Renew old certificates:
            
            certbot renew

9.  **Build configured *Najdispoj* image**

        docker compose build --build-arg override=customconfig [--no-cache]

10. **Run *Najdispoj***

        docker compose up -d
    
    - When deploying for the first time, run without *-d* to see error messages.
    - If you are running Najdispoj on Windows, you might need to increase Docker's memory limit (in `.wslconfig` file) to be able to build the transit graph.

11. **(Optional)** Run *Najdispoj* on server boot and restart on failure

    - Create a file called `najdispoj.service` in `/etc/systemd/system` and paste the following inside:

            [Unit]
            Description=Najdispoj Trip Planner
            [Service]
            User=najdispoj
            Restart=on-failure
            RestartSec=30
            ExecStart=/home/najdispoj/najdispoj/run.sh
            [Install]
            WantedBy=multi-user.target
    
    - Enter the following command:

            sudo systemctl enable najdispoj


    - You may need to change permissions of the `run.sh` file:

            chmod +x /home/najdispoj/najdispoj/run.sh
    
    - Modify build command in `run.sh` to include the override from step 9

## Development guide

*Docker* is required for development.

*Najdispoj* has been developed in *Visual Studio Code* inside a development container. It is possible to develop *Najdispoj* both on local and remote dev container (the latter of which also being useful for troubleshooting while setting up your instance). The configuration files required by *Visual Studio Code* are included in the repository.

Inside the dev container, run *Najdispoj* using the following command (`-dev` parameter enables reload-on-save):

    python server/run.py -dev

### Developing the client

Inside the dev container, run the following command from the `/client` directory:

    npm run serve

More commands are described in the README.md file inside the directory.

### Debugging a Najdispoj-managed container

Attach *VS Code* to the running container and kill the running process (find its process id using *top* command). Run the process manually with console output (the full command can be found in najdispoj_server's console output).

### Disabling SSL

To disable SSL (e.g. when developing locally), leave the "https" config uncommented at the bottom of `/containers/Nginx/Dockerfile`. You should also change the network port to 80 (or other) in `/server/config.py`.

### Increasing file watchers limit

You might want to increase the system limit for number of file watchers (otherwise the `npm run serve` command could fail). On the host machine (**Important: Not inside a *Docker* dev container**) open the terminal as user *najdispoj* and enter the following command:

    echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sysctl -p

![Object model diagram](static/diagram.svg)