from server.core.coords import Coords
from enum import Enum, auto
import time


class VehicleType(Enum):
    BUS = "bus"
    TROLLEY = "trolley"
    TRAM = "tram"


class Vehicle:
    """Represents a moving vehicle marker on the map."""

    def __init__(
        self,
        number: int,
        coords: Coords | None = None,
        velocity: int | None = None,
        heading: int | None = None,
        line: str | None = None,
        type: VehicleType | None = None,
    ):
        self._number = number
        self._coords = coords
        self._velocity = velocity
        self._heading = heading
        self._line = line
        self._type = type
        self._last_change = int(time.time())

    def changed(self):
        """Sets the last changed time to current UTC time in seconds."""
        self._last_change = int(time.time())

    def get_last_change(self):
        return self._last_change

    def get_number(self):
        return self._number

    def get_coords(self):
        return self._coords

    def set_coords(self, coords: Coords) -> None:
        self._coords = coords
        self.changed()

    def get_line(self) -> str | None:
        return self._line

    def set_line(self, line: str | None) -> None:
        self._line = line
        self.changed()

    def get_type(self) -> VehicleType | None:
        return self._type

    def set_type(self, type: VehicleType | None) -> None:
        self._type = type
        self.changed()

    def get_velocity(self) -> int | None:
        return self._velocity

    def set_velocity(self, velocity: int | None) -> None:
        self._velocity = velocity
        self.changed()

    def get_heading(self) -> int | None:
        return self._heading

    def set_heading(self, heading: int | None) -> None:
        self._heading = heading
        self.changed()

    def to_json(self) -> dict:
        coords = self.get_coords().to_json() if self.get_coords() else None

        return {
            "number": self.get_number(),
            "coords": coords,
            "velocity": self._velocity,
            "heading": self._heading,
            "line": self.get_line(),
            "type": self.get_type(),
            "lastChange": self.get_last_change(),
        }

    def __str__(self) -> str:
        return str(vars(self))
