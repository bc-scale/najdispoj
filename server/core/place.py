from typing import TypedDict
from enum import Enum
from server.core.coords import Coords


class PlaceName(TypedDict):
    primary: str
    secondary: str


class PlaceType(Enum):
    STREET = "street"
    SHOP = "shop"
    INSTITUTION = "institution"
    STOP = "stop"
    TOWN = "town"
    BUILDING = "building"
    OTHER = "other"


class Place:
    def __init__(
        self, name: PlaceName, coords: Coords, type_: PlaceType | None = None
    ):
        self._name = name
        self._coords = coords
        self._type = type_

    def get_name(self) -> PlaceName:
        return self._name

    def get_coords(self) -> Coords:
        return self._coords

    def get_type(self) -> PlaceType | None:
        return self._type

    def to_json(self) -> dict:
        """
        Return an object, which can be automatically converted to JSON by
        FastAPI and sent to a client in JSON format.
        """
        return {
            "name": self._name,
            "coords": self._coords.to_json(),
            "type": self._type.value if self._type else None,
        }

    def __str__(self) -> str:
        return (
            self._name["primary"]
            + ", "
            + self._name["secondary"]
            + ": "
            + str(self._coords)
        )
