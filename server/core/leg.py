from enum import Enum
from server.core.stop import Stop


class LegMode(Enum):
    WALK = "walk"
    BUS = "bus"
    TRAM = "tram"
    TROLLEY = "trolley"
    TRAIN = "train"
    UNKNOWN = "unknown"


class Leg:
    """Represents a segment of an itinerary."""

    def __init__(
        self,
        mode: LegMode,
        duration: int,
        distance: int,
        path: str,
        departure: int,
        arrival: int,
        number: str,
        short_name: str,
        headsign: str,
        stops: list[Stop],
    ):
        self._mode = mode
        self._duration = duration
        self._distance = distance
        self._path = path
        self._departure = departure
        self._arrival = arrival
        self._number = number
        self._short_name = short_name
        self._headsign = headsign
        self._stops = stops
        self._waiting_duration: int = 0
        self._timeline_duration: int | None = None

    def get_mode(self) -> LegMode:
        return self._mode

    def get_duration(self) -> int:
        return self._duration

    def get_departure(self) -> int:
        return self._departure

    def get_arrival(self) -> int:
        return self._arrival

    def get_stops(self) -> list[Stop]:
        return self._stops

    def get_total_duration(self) -> int:
        return self._duration + self._waiting_duration

    def compute_timeline_duration(self, max_duration) -> None:
        self._timeline_duration = round(
            self.get_total_duration() / max_duration * 100, 2
        )

    def set_waiting_duration(self, duration) -> None:
        self._waiting_duration = duration

    def to_json(self) -> dict:
        """
        Returns an object, which can be automatically converted to JSON by
        FastAPI and sent to a client in JSON format.
        """
        return {
            "mode": self._mode.value,
            "duration": self._duration,
            "distance": self._distance,
            "path": self._path,
            "departure": self._departure,
            "arrival": self._arrival,
            "number": self._number,
            "shortName": self._short_name,
            "headsign": self._headsign,
            "stops": list(map(lambda stop: stop.to_json(), self._stops)),
            "timelineDuration": self._timeline_duration,
            "waitingDuration": self._waiting_duration,
            "totalDuration": self.get_total_duration(),
        }
