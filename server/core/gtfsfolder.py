import pandas
import os
import logging
import zipfile
import glob
import csv
import unicodedata
import shutil
from server.core.osmfile import OSMFile

import server.config as config


class GTFSFolder:
    def __init__(self, path: str):
        """
        Manager of a GTFS folder.

        Provides basic tools for working with GTFS data.

        Args:
            path (str): The path to GTFS data folder.
        """
        self.path = path
        self.label = "default_label"

    def load_zip(self, path: str):
        logging.info(f"🚉 Loading zip from path {path} to {self.path}...")

        with zipfile.ZipFile(path, "r") as file:
            file.extractall(self.path)

        logging.info("✅ Done.")
        return self

    def use_dataframe(self) -> pandas.DataFrame:
        """Returns a Pandas.DataFrame object with GTFS data."""
        return pandas.concat(
            map(pandas.read_csv, glob.glob(self.path + "/*.csv"))
        )

    def repair_feed_info(self, url: str):
        """
        Remove accents from feed info - accents cause an error in
        OpenTripPlanner.

        Adds URL of feed provider (required by OpenTripPlanner).
        """
        logging.info("🚉 Repairing feed info...")

        with (
            open(self.path + "/feed_info.txt", "r") as file,
            open(self.path + "/feed_info.txt.tmp", "w+") as tmp,
        ):
            reader = csv.DictReader(file)
            fieldnames = (
                list(reader.fieldnames) if reader.fieldnames else list()
            )
            writer = csv.DictWriter(tmp, fieldnames=fieldnames)

            writer.writeheader()
            for row in reader:
                # Remove accents
                row["feed_publisher_name"] = (
                    unicodedata.normalize("NFKD", row["feed_publisher_name"])
                    .encode("ascii", "ignore")
                    .decode("ascii")
                )
                # Set URL
                row["feed_publisher_url"] = url
                writer.writerow(row)

        os.remove(self.path + "/feed_info.txt")
        os.rename(
            self.path + "/feed_info.txt.tmp",
            self.path + "/feed_info.txt",
        )

        logging.info("✅ Done.")
        return self

    def repair_stops(self):
        """
        The specified parent stations are not defined. To solve the problem we
        can simply remove them.
        """
        logging.info("🚉 Repairing stops...")

        with (
            open(self.path + "/stops.txt", "r") as file,
            open(self.path + "/stops.txt.tmp", "w") as tmp,
        ):
            reader = csv.DictReader(file)
            fieldnames = (
                list(reader.fieldnames) if reader.fieldnames else list()
            )
            writer = csv.DictWriter(tmp, fieldnames=fieldnames)

            writer.writeheader()
            for row in reader:
                row["parent_station"] = ""
                writer.writerow(row)

        os.remove(self.path + "/stops.txt")
        os.rename(self.path + "/stops.txt.tmp", self.path + "/stops.txt")

        logging.info("✅ Done.")
        return self

    def change_route_types(self, type_from, type_to):
        """
        800 is the trolleybus route_type in Extended GTFS Route Types:
        https://developers.google.com/transit/gtfs/reference/extended-route-types
        11 is the route_type according to these docs:
        https://developers.google.com/transit/gtfs/reference#routestxt
        OTP 1.5.0 refuses to build graph unless this gets replaced
        """
        # TODO look deeper into this issue
        logging.info("🚉 Repairing routes...")

        with (
            open(self.path + "/routes.txt", "r") as file,
            open(self.path + "/routes.txt.tmp", "w") as tmp,
        ):
            reader = csv.DictReader(file)
            fieldnames = (
                list(reader.fieldnames) if reader.fieldnames else list()
            )
            writer = csv.DictWriter(tmp, fieldnames=fieldnames)

            writer.writeheader()
            for row in reader:
                if row["route_type"] == type_from:
                    row["route_type"] = type_to
                writer.writerow(row)

        os.rename(self.path + "/routes.txt", self.path + "routes.txt.old")
        os.rename(self.path + "/routes.txt.tmp", self.path + "/routes.txt")

        logging.info("✅ Done.")
        return self

    def revert_routes(self):
        logging.info("🚉 Reverting routes...")

        os.remove(self.path + "routes.txt")
        os.rename(self.path + "routes.txt.old", self.path + "routes.txt")

        logging.info("✅ Done.")
        return self

    def enhance(self, osm_file: OSMFile):
        """Create shape files using Pfaedle."""
        logging.info("🚉 Enhancing GTFS data...")

        cmd = (
            f"{config.pfaedle_path} -D --inplace -x {osm_file.path} {self.path}"
        )

        logging.info(f"🏃 Running command: {cmd}")
        os.system(cmd)

        logging.info("✅ Done.")
        return self

    def zip(self, zip_path):
        """Zip GTFS data"""
        logging.info(f"🚉 Zipping GTFS data from {self.path} to f{zip_path}...")

        shutil.make_archive(os.path.splitext(zip_path)[0], "zip", self.path)

        logging.info("✅ Done.")
        return self

    def copy(self, path: str):
        """
        Copies GTFS data to <path> and return the copy's GTFSFolder object.
        """
        logging.info("🚉 Copying GTFS data from %s to %s...", self.path, path)

        shutil.copytree(self.path, path, dirs_exist_ok=True)

        logging.info("✅ Done.")
        return GTFSFolder(path).set_label(f"{self.label} - copy")

    def set_label(self, label: str):
        """
        The label used e.g. when naming related zip files.

        Args:
            label (str): ASCII string
        """
        logging.info(f"🚌 Setting label to {label}")

        self.label = label

        logging.info("✅ Done.")
        return self
