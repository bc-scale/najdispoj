from server.core.gtfsfolder import GTFSFolder


class IStaticDataProvider:
    """
    An informal interface for static data providers - not all functions have to be
    overridden by a provider class.
    """

    def load_data(self, path: str) -> GTFSFolder | None:
        raise NotImplementedError()
