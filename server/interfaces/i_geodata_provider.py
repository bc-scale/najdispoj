from server.core.bounding_box import BoundingBox
from server.core.osmfile import OSMFile


class IGeodataProvider:
    """
    An informal interface for geodata providers - not all functions have to be
    overridden by a provider class.
    """

    def load_data(self, path: str, **kwargs) -> OSMFile | None:
        raise NotImplementedError()
