import requests
import json

from server.interfaces.i_geocoding_provider import IGeocodingProvider
from server.core.place import Place, PlaceType
from server.core.coords import Coords

from server.utils.geojson import GeoJSON
from server.utils.configtools import ConfigTools


class PhotonProvider(IGeocodingProvider):
    """
    Provider of geocoding data from Komoot Photon
    https://github.com/komoot/photon
    """

    def __init__(self, address):
        # IP address and port of a Photon API instance.
        # (Without the trailing '/')
        self.address = address

    def reverse_geocode(self, coords: Coords) -> Place:
        params = {"lat": str(coords.get_lat()), "lon": str(coords.get_lng())}

        data = requests.get(self.address + "/reverse", params=params).json()

        return Place(GeoJSON.get_name(data["features"][0]), coords)

    @staticmethod
    def sort_places(results: list[Place]) -> None:
        """Sorts a list of Places in-place."""

        def sortFunc(place: Place):
            typeRatings = {
                PlaceType.STOP: 0,
                PlaceType.INSTITUTION: 1,
                PlaceType.SHOP: 2,
                PlaceType.BUILDING: 3,
                PlaceType.STREET: 4,
                PlaceType.OTHER: 5,
            }

            place_type = place.get_type()
            if place and place_type in typeRatings:
                return typeRatings[place_type]
            else:
                return 100

        results.sort(key=sortFunc)

    def autocomplete(self, query: str) -> list[Place]:
        bbox = ConfigTools.get_bbox()
        center = ConfigTools.get_center()

        params: dict = {
            "q": query,
            "limit": 10,
            "bbox": str(bbox),
            "lat": center.get_lat(),
            "lon": center.get_lng(),
            "location_bias_scale": 4,
            "osm_tag": [
                "!boundary",
                ":!unclassified",
                # ":!town",
                # ":!neighbourhood",
            ],
        }

        data = requests.get(self.address + "/api", params=params).json()

        used_ids: list[int] = []
        results: list[Place] = []

        print("Data:", json.dumps(data, indent=2))
        features = data["features"]
        for place in features:
            coords = Coords(
                place["geometry"]["coordinates"][1],
                place["geometry"]["coordinates"][0],
            )

            place_id = place["properties"]["osm_id"]

            if place_id in used_ids:
                continue

            used_ids.append(place_id)

            results.append(
                Place(GeoJSON.get_name(place), coords, GeoJSON.get_type(place))
            )

        self.sort_places(results)

        return results
