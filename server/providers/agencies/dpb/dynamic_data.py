from threading import Thread
from server.core.coords import Coords
from server.core.vehicle import Vehicle, VehicleType
import socket
from server.interfaces.i_dynamic_data_provider import IDynamicDataProvider
from .messages import (
    MessageType,
    StatusMessage,
    StopCourseMessage,
    StopHeadingMessage,
    GPSMessage,
)
import logging

Vehicles = dict[int, Vehicle]


class DPBDynamicDataProvider(IDynamicDataProvider):
    """
    Provider of dynamic vehicle data from DPB (Radiopol).

        Args:
            address (str): Address to listen on.
            port (int): Port to listen on.
            forward_raw_to (tuple): Address and port to forward all raw packets
                to in format (address: str, port: int)
    """

    def __init__(
        self,
        address: str = "0.0.0.0",
        port: int = 50802,
        forward_raw_to: tuple[str, int] | None = None,
    ):
        self._address = address
        self._port = port
        self._vehicles: Vehicles = {}
        self._forward_raw_to = forward_raw_to

        self.start_listener()

    def start_listener(self):
        def listen():
            buffer_size = 1024

            while True:
                try:
                    data, _ = self._socket.recvfrom(buffer_size)
                    if self._forward_raw_to:
                        self._socket.sendto(data, self._forward_raw_to)
                    self.handle_packet(data)
                except socket.error as exc:
                    logging.error("socket.error:", exc)

        logging.info("Starting dynamic data listener...")

        self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._socket.bind((self._address, self._port))
        self.listener = Thread(target=listen)
        # Without the following line, the listener thread cannot be
        # killed using Ctrl+C
        self.listener.setDaemon(True)
        self.listener.start()

    def get_vehicles(self) -> Vehicles:
        """
        Returns a dictionary of vehicles.
        Optionally filter from vehicles which don't have coords or their line
        number filled in (filter_incomplete).
        """

        return self._vehicles

    def get_add_vehicle(self, vehicle_no: int) -> Vehicle:
        """
        Gets vehicle by its number; if it doesn't exist, creates a new vehicle,
        adds it to self._vehicles and returns it.
        """
        if vehicle_no in self.get_vehicles():
            return self._vehicles[vehicle_no]
        else:
            new_vehicle = Vehicle(vehicle_no)
            self.add_vehicle(new_vehicle)
            return new_vehicle

    def get_vehicle(self, vehicle_no: int) -> Vehicle | None:
        if vehicle_no in self.get_vehicles():
            return self._vehicles[vehicle_no]
        else:
            return None

    def add_vehicle(self, vehicle) -> None:
        self._vehicles[vehicle.get_number()] = vehicle

    def get_message_type(self, data) -> MessageType:
        match data[4:6]:
            case "03":
                return MessageType.STATUS
            case "0b":
                return MessageType.STOPCOURSE
            case "0c":
                return MessageType.STOPHEADING
            case "83":
                return MessageType.GPS
            case _:
                return MessageType.UNKNOWN

    def handle_packet(self, data) -> None:
        hexdata = data.hex()
        match self.get_message_type(hexdata):
            case MessageType.STATUS:
                status_msg = StatusMessage(hexdata)
                if status_msg.line:
                    vehicle = self.get_add_vehicle(status_msg.vehicle_no)
                    vehicle.set_line(status_msg.line)
                    vehicle.set_type(
                        DPBDynamicDataProvider.get_type_by_line(status_msg.line)
                    )

            case MessageType.STOPCOURSE:
                stop_course_msg = StopCourseMessage(hexdata)
                if stop_course_msg.line:
                    vehicle = self.get_add_vehicle(stop_course_msg.vehicle_no)
                    vehicle.set_line(stop_course_msg.line)
                    vehicle.set_type(
                        DPBDynamicDataProvider.get_type_by_line(
                            stop_course_msg.line
                        )
                    )

            case MessageType.STOPHEADING:
                stop_heading_msg = StopHeadingMessage(hexdata)
                if stop_heading_msg.line:
                    vehicle = self.get_add_vehicle(stop_heading_msg.vehicle_no)
                    vehicle.set_line(stop_heading_msg.line)
                    vehicle.set_type(
                        DPBDynamicDataProvider.get_type_by_line(
                            stop_heading_msg.line
                        )
                    )

            case MessageType.GPS:
                try:
                    gps_msg: GPSMessage = GPSMessage(hexdata)
                    vehicle = self.get_add_vehicle(gps_msg.vehicle_no)
                    vehicle.set_coords(
                        Coords(gps_msg.latitude, gps_msg.longitude)
                    )
                    vehicle.set_velocity(gps_msg.velocity)
                    vehicle.set_heading(gps_msg.heading)

                except ValueError:
                    # TODO probably just a packet with all relevances turned off
                    # logging.info(
                    #     "Received a corrupt packet, unable to create a datetime."
                    # )
                    # print("ERROR:", hexdata)
                    return
            case MessageType.UNKNOWN:
                print("UNKNOWN:", hexdata)

    @staticmethod
    def get_type_by_line(line: str) -> VehicleType | None:
        """
        TODO create a GTFS analyzer (Static Data Provider?)
        (https://pypi.org/project/gtfs-kit/)
        get types from the analyzer by line number
        """

        if not line:
            return None

        match len(line):
            case 1:
                if line == "0":
                    # Vehicle transfer (?)
                    return None
                else:
                    return VehicleType.TRAM
            case 2:
                return VehicleType.BUS
            case 3:
                if line[0] == "N":
                    return VehicleType.BUS
                else:
                    return VehicleType.TROLLEY
            case _:
                return None
