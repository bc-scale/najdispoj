from dataclasses import dataclass
from datetime import datetime, date
from enum import Enum, auto


class MessageType(Enum):
    STATUS = auto()
    STOPCOURSE = auto()
    STOPHEADING = auto()
    GPS = auto()
    UNKNOWN = auto()


@dataclass
class AbstractMessage:
    def __init__(self, data: str):
        self.data_length: int = int(data[0:4], 16)
        self.code: str = data[4:6]
        self.vehicle_no: int = int(data[6:10], 16)

    @staticmethod
    def fix_line(line) -> str | None:
        """
        Since the packets are received in binary format (hex), the line
        number is coded as a number. This function fixes a few special
        cases, in which the line number has been changed.
        """
        if line == "0":
            return None

        if len(line) == 3:
            if line[0] == "4":
                return "N" + line[1:]

            # TODO make a universal rule?
            if line == "870":
                return "X70"
            if line == "831":
                return "X31"

        return line


@dataclass
class StatusMessage(AbstractMessage):
    def __init__(self, data: str):
        super().__init__(data)

        self.line: str | None = AbstractMessage.fix_line(
            str(int(data[10:14], 16))
        )
        self.order: int = int(data[14:16], 16)

    def __str__(self) -> str:
        return "STATUS: " + str(vars(self))


@dataclass
class StopCourseMessage(AbstractMessage):
    def __init__(self, data: str):
        super().__init__(data)

        today = date.today()
        self.time_relevance: int = int(data[10:12], 16)
        self.datetime: datetime = datetime(
            today.year,  # year
            today.month,  # month
            today.day,  # day
            int(data[10:12], 16),  # hour
            int(data[12:14], 16),  # minute
            int(data[14:16], 16),  # second
        )
        self.line: str | None = AbstractMessage.fix_line(
            str(int(data[16:20], 16))
        )
        self.course: str = data[20:22]
        self.service: str = data[22:28]
        self.stop: str = data[28:32]
        self.arrival_type: int = int(data[32:33], 16)
        self.stop_type: int = int(data[33:34], 16)

    def __str__(self) -> str:
        return "COURSE: " + str(vars(self))


@dataclass
class StopHeadingMessage(AbstractMessage):
    def __init__(self, data: str):
        super().__init__(data)

        today = date.today()
        self.code: str = data[4:6]
        self.vehicle_no: int = int(data[6:10], 16)
        self.time_relevance: int = int(data[10:12], 16)
        self.datetime: datetime = datetime(
            today.year,  # year
            today.month,  # month
            today.day,  # day
            int(data[10:12], 16),  # hour
            int(data[12:14], 16),  # minute
            int(data[14:16], 16),  # second
        )
        self.line: str | None = AbstractMessage.fix_line(
            str(int(data[16:20], 16))
        )
        self.heading: str = data[20:22]
        self.service: str = data[22:28]
        self.stop: str = data[28:32]
        self.arrival_type: int = int(data[32:33], 16)
        self.stop_type: int = int(data[33:34], 16)

    def __str__(self) -> str:
        return "HEADING: " + str(vars(self))


class GPSMessage(AbstractMessage):
    def __init__(self, data: str):
        super().__init__(data)

        self.time_relevance: int = int(data[10:12], 16)
        self.datetime: datetime = datetime(
            2000 + int(data[12:14], 16),  # year
            int(data[14:16], 16),  # month
            int(data[16:18], 16),  # day
            int(data[18:20], 16),  # hour
            int(data[20:22], 16),  # minute
            int(data[22:24], 16),  # second
        )

        self.location_relevance: int = int(data[24:26], 16)
        self.latitude: float = int(data[26:34], 16) / 1000000
        self.longitude: float = int(data[34:42], 16) / 1000000
        self.velocity_relevance: int = int(data[42:44], 16)
        self.velocity: int = int(data[44:46], 16)
        self.heading_relevance: int = int(data[46:48], 16)
        self.heading: int = int(data[48:50], 16) * 2

    def __str__(self) -> str:
        return "GPS: " + str(vars(self))
