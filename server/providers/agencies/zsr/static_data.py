import logging
import os
import urllib.request
import urllib.error
import requests
from io import StringIO
from bs4 import BeautifulSoup

from server.interfaces.i_static_data_provider import IStaticDataProvider
from server.core.gtfsfolder import GTFSFolder


class ZSRStaticDataProvider(IStaticDataProvider):
    """
    Provider of raw GTFS data from DPMO (Dopravní podnik města Olomouce, a.s.)
    """

    def download_data(self, path: str) -> GTFSFolder | None:
        """Download GTFS data to the given path."""
        gtfs_url = f"https://www.zsr.sk/files/pre-cestujucich/cestovny-poriadok/gtfs/gtfs.zip"
        zip_path = f"{path}gtfs.zip"

        logging.info(f"🚉ZSR Downloading GTFS data from {gtfs_url}...")
        try:
            os.makedirs(os.path.dirname(zip_path), exist_ok=True)
            with open(zip_path, "wb+") as file:
                # urllib.urlretrieve didn't work (due to SSL verification)
                response = requests.get(gtfs_url)
                file.write(response.content)
        except requests.exceptions.RequestException as e:
            logging.info(f"❌ Error: {e}")
            logging.info("❌ Download has failed.")
            return None

        logging.info("✅ Done.")

        return GTFSFolder(path).set_label("ZSR").load_zip(zip_path)

    def load_data(self, path: str) -> GTFSFolder | None:
        gtfs_folder = self.download_data(path)
        return gtfs_folder
        # return gtfs_folder.change_route_types("11", "800")
        # return gtfs_folder.repair_feed_info("https://dpmo.cz/")
