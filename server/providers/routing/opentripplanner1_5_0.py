import json
import requests
import logging

from server.core.coords import Coords
from server.core.stop import Stop
from server.core.leg import Leg, LegMode
from server.core.itinerary import Itinerary
from server.core.plan import Plan

from server.interfaces.i_routing_provider import IRoutingProvider

import server.config as config


class OpenTripPlanner1_5_0Provider(IRoutingProvider):
    def __init__(self, address, region: str = "default"):
        # IP address and port of an OpenTripPlanner 1.5.0 instance
        # (Without the trailing /)
        self.address = address
        self.region = region

    def _create_stop(self, stop, last=False):
        def stop_name(n):
            if n == "Origin" or n == "Destination":
                return ""
            return n

        return Stop(
            stop_name(stop["name"]),
            Coords(stop["lat"], stop["lon"]),  # TODO maybe convert to float
            stop["arrival"] if last else stop["departure"],
        )

    def _leg_mode(self, route_type: int | None) -> LegMode:
        mode_by_route_type = {
            0: LegMode.TRAM,
            2: LegMode.TRAIN,
            3: LegMode.BUS,
            800: LegMode.TROLLEY,
        }

        if route_type == None:
            return LegMode.WALK

        if route_type in mode_by_route_type:
            return mode_by_route_type[route_type]

        return LegMode.UNKNOWN

    def _create_leg(self, leg, last):

        if last:
            duration = leg["duration"] - 60
        else:
            duration = leg["duration"]

        stops = []
        stops.append(self._create_stop(leg["from"]))
        if "intermediateStops" in leg:
            for stop in leg["intermediateStops"]:
                stops.append(self._create_stop(stop))
        stops.append(self._create_stop(leg["to"], True))

        number = ""
        if "routeShortName" in leg:
            number = leg["routeShortName"]

        short_name = leg["tripShortName"] if "tripShortName" in leg else ""
        headsign = leg["headsign"] if "headsign" in leg else ""

        route_type = leg["routeType"] if "routeType" in leg else None

        return Leg(
            self._leg_mode(route_type),
            duration,
            leg["distance"],
            leg["legGeometry"]["points"],
            leg["startTime"],
            leg["to"]["arrival"],
            number,
            short_name,
            headsign,
            stops,
        )

    def _create_itinerary(self, itinerary):
        legs = []
        for index, leg in enumerate(itinerary["legs"]):
            last = len(legs) - 1 == index
            legs.append(self._create_leg(leg, last))

        return Itinerary(
            itinerary["duration"],
            itinerary["startTime"],
            itinerary["endTime"],
            legs,
        )

    def _create_plan(self, data):
        if ("plan" not in data) or ("itineraries" not in data["plan"]):
            return Plan()

        itineraries = []
        for itinerary in data["plan"]["itineraries"]:
            itineraries.append(self._create_itinerary(itinerary))

        return Plan(itineraries)

    def get_plan(self, params):
        # TODO specify a class (interface) for params
        # TODO if no itineraries found, increase maxWalkDistance, repeat
        walkSpeedOptions = [0.8, 1.3, 2.22, 3.5]  # meters/second
        optimizeOptions = {"quick": "QUICK", "transfers": "TRANSFERS"}

        # TODO include these parameters in query:
        # wheelchair, locale

        parameters: dict = {
            "fromPlace": str(params["origin"]),
            "toPlace": str(params["destination"]),
            "time": params["time"],
            "date": params["date"],
            "showIntermediateStops": True,
            "arriveBy": params["arrive_by"],
            "numItineraries": 5,
            "minTransferTime": 120,
            # "maxTransfers": params["max_transfers"],
            "walkSpeed": walkSpeedOptions[params["walk_speed"]],
            "optimize": optimizeOptions[params["optimize"]],
            "mode": "TRANSIT,WALK",
            "transferSlack": 0,
            "boardSlack": 0,
            "alightSlack": 0,
        }

        if params["max_walk_distance"] != 0:
            parameters["maxWalkDistance"] = params["max_walk_distance"]

        # Get data from OTP
        data = requests.get(
            self.address + f"/otp/routers/{self.region}/plan?",
            params=parameters,
        )
        logging.info(f"OpenTripPlanner GET url: {data.url}")
        data = json.loads(data.content)

        plan = self._create_plan(data)
        plan.compute_timelines()  # TODO maybe move to _create_plan()

        if config.debug_mode:
            plan.set_raw(data)

        return plan
