import logging
import time

from server.interfaces.i_dynamic_data_provider import IDynamicDataProvider
from server.core.vehicle import Vehicle

import server.config as cfg


class DynamicDataService:
    """
    TODO differentiate between downloading and "applying" (setting as currently
    used) GTFS data. Add config parameter "preserve old datasets".
    """

    def __init__(self):
        self.providers: dict[str, IDynamicDataProvider] = {}

    def register_provider(self, name: str, provider: IDynamicDataProvider):
        """Registers and initializes a dynamic data provider."""
        logging.info("➕ Registered a dynamic data provider: %s", name)
        self.providers[name] = provider
        return self

    def get_vehicles(self) -> dict[str, Vehicle]:
        """Get positions of all vehicles."""
        vehicles = self.providers["DPB"].get_vehicles()
        return dict(  # TODO move to a filter function
            filter(
                lambda v: v[1].get_coords()
                and v[1].get_line()
                and v[1].get_last_change() > time.time() - 300,
                vehicles.items(),
            )
        )
