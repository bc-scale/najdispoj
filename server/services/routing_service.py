import logging

from server.core.plan import Plan
from server.interfaces.i_routing_provider import IRoutingProvider


class RoutingService:
    def __init__(self, static_data_service, geodata_service):
        self.static_data_service = static_data_service
        self.geodata_service = geodata_service
        self.providers: dict[str, IRoutingProvider] = {}

    def register_provider(self, name: str, provider: IRoutingProvider):
        """Registers and initializes a routing provider."""
        self.providers[name] = provider
        logging.info("New routing service: %s", name)
        return self

    def get_plan(self, params) -> Plan:
        return self.providers["OpenTripPlanner 1.5.0"].get_plan(params)
