import logging

from server.interfaces.i_geocoding_provider import IGeocodingProvider
from server.core.coords import Coords
from server.core.place import Place


class GeocodingService:
    def __init__(self):
        self.providers: dict[str, IGeocodingProvider] = {}

    def register_provider(self, name: str, provider: IGeocodingProvider):
        """Registers and initializes a geocoding provider."""
        logging.info("➕ Registered a geocoding provider: %s", name)
        self.providers[name] = provider
        return self

    def autocomplete(self, query: str) -> list[Place]:
        """Returns places that match the query string."""
        return self.providers["Photon"].autocomplete(query)

    def reverse_geocode(self, coords: Coords) -> Place:
        """Returns the name of a place based on its coordinates."""
        return self.providers["Photon"].reverse_geocode(coords)
