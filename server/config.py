"""
Default config - do not modify

Put the modified file into /overrides/<override>/server/

Provider-independent variables are set in this file.
Provider-dependent variables should be set in the provider's
constructor (in main.py).

This file should mostly contain information about the deployment environment.
"""

network = {
    "host": "localhost",
    "port": 8081,  # 443 for https, 80 for http
    # Nginx -> Najdispoj Server - also redefine in docker-compose.yml
    "najdispoj_port": 25516,
    "dynamic_data_port": 50801,
}

area = {
    "bbox": (
        {"lat": 47.699, "lng": 16.815},
        {"lat": 49.767, "lng": 22.6},
    ),
    "center": {"lat": 48.1742, "lng": 17.1527},
}

debug_mode = False
pfaedle_path = "/usr/src/app/vendor/pfaedle/build/pfaedle"
