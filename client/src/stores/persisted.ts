import { defineStore } from "pinia";
import { PlanPlaces } from "@/core/Plan";
import isEqual from "lodash.isequal";
import Itinerary from "@/core/Itinerary";

const MAX_RECENT_LENGTH = 5;

export const usePersistedStore = defineStore("persisted", {
    state: () => ({
        recentPlans: [] as PlanPlaces[],
        favoritePlans: [] as PlanPlaces[],
        savedItineraries: [] as Itinerary[],
        allowGeolocation: false as boolean,
    }),
    persist: true,
    actions: {
        toggleRememberPlan(planPlaces) {
            // If plan is already "favorited", don't add it to recent
            let isFavorite = false;
            this.favoritePlans.forEach(function (params) {
                if (isEqual(params, planPlaces)) {
                    isFavorite = true;
                    return;
                }
            });
            if (isFavorite) {
                return;
            }

            // If plan is already in recent plans, don't add it again
            let isRecent = false;
            this.recentPlans.forEach(function (params) {
                if (isEqual(params, planPlaces)) {
                    isRecent = true;
                    return;
                }
            });
            if (isRecent) {
                return;
            }

            // Add PlanPlaces to recent plans
            this.recentPlans.push(planPlaces);

            // Trim size of recent plans
            this.recentPlans = this.recentPlans.slice(
                Math.max(this.recentPlans.length - MAX_RECENT_LENGTH, 0)
            );
        },

        toggleFavoritePlan(planPlaces) {
            // Toggle - favorite/unfavorite

            // If is "favorited"
            let isFavorite = false;
            this.favoritePlans.forEach(function (params) {
                if (isEqual(params, planPlaces)) {
                    isFavorite = true;
                    return;
                }
            });

            if (isFavorite) {
                // Remove from favorites
                var index = this.favoritePlans.indexOf(planPlaces);
                if (index !== -1) {
                    this.favoritePlans.splice(index, 1);
                }
            } else {
                // Add to favorites
                this.favoritePlans.push(planPlaces);
            }
        },

        saveItinerary(itinerary) {
            var skip = false;
            // If is already saved, skip (this shouldn't ever happen though)
            this.savedItineraries.forEach(function (it) {
                if (isEqual(it, itinerary)) {
                    skip = true;
                    return;
                }
            });
            if (skip) {
                return;
            }

            this.savedItineraries.push(itinerary);
        },

        unsaveItinerary(itinerary) {
            this.savedItineraries = this.savedItineraries.filter((it) => {
                return !isEqual(it, itinerary);
            });
        },

        clearSaved() {
            this.savedItineraries = [];
        },
    }
});
