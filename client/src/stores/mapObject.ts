import { defineStore } from "pinia";
import config from "../../config";
import { createJSONFetch } from "../utils/fetch";
import Vehicle from "../core/Vehicle";

export const useMapObjectStore = defineStore("mapObject", {
  state: () => ({
    vehicles: [] as Vehicle[],
  }),
  actions: {
    fetchVehicles(callback?) {
      createJSONFetch(config.api.host + "/vehicles").then((vehicles) => {
        this.vehicles = vehicles.map((vehicle) => new Vehicle(vehicle));
        if (callback) {
          callback();
        }
      });
    },
  },
});
