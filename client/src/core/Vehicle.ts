import Coords, { CoordsJSON } from "./Coords";

export enum VehicleType {
  Bus = "bus",
  Trolley = "trolley",
  Tram = "tram",
}

export type VehicleMap = Map<number, Vehicle>;

export type VehicleJSON = {
  number: number;
  coords: CoordsJSON | null;
  velocity: number | null;
  heading: number | null;
  line: string | null;
  type: VehicleType | null;
  lastChange: number | null;
};

/**
 * Represents a vehicle marker on the map.
 */
export default class Vehicle {
  readonly number: number;
  readonly coords: Coords | null;
  readonly velocity: number | null;
  readonly heading: number | null;
  readonly line: string | null;
  readonly type: VehicleType | null;
  readonly lastChange: number | null;

  constructor({
    number,
    coords = null,
    velocity = null,
    heading = null,
    line = null,
    type = null,
    lastChange = null,
  }: VehicleJSON) {
    this.number = number;
    this.coords = coords ? new Coords(coords) : null;
    this.velocity = velocity;
    this.heading = heading;
    this.line = line;
    this.type = type;
    this.lastChange = lastChange;
  }
}
