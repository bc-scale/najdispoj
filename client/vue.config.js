module.exports = {
  lintOnSave: false,
  css: {
    loaderOptions: {
      // pass options to sass-loader
      scss: {
        // @/ is an alias to src/
        prependData: `
          @use "sass:math";
          @import "@/variables.scss";
        `,
      },
    },
  },
  configureWebpack: {
    devServer: {
      allowedHosts: "all", // Not a security threat, since all the traffic gets routed through Nginx
      host: '0.0.0.0',
    },
    watchOptions: {
      aggregateTimeout: 500, // Short delay before reloading
      poll: 1000, // Leave uncommented if "npm run serve" is not reloading properly
      ignored: '**/node_modules',
    },
  },
};
