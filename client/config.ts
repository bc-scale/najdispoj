/**
 * Default config - do not modify
 * 
 * Put the modified file into /overrides/<override>/client/
 */

// Bratislava
export default {
    title: 'Najdispoj',
    ticket: false,
    api: {
        host: 'http://localhost:8081/api'
    },
    locales: {
        default: 'sk',
        fallback: 'en',
        supported: ['en', 'sk']
    },
    map: {
        vehicles: true,
        center: {
            lat: 48.1625,
            lng: 17.1225
        },
        zoom: 13.5,
    },
    sources: [
        {
            "name": "Dopravný podnik Bratislava, a.s.",
            "url": "https://dpb.sk/"
        },
        {
            "name": "Dopravní podnik města Olomouce, a.s.",
            "url": "https://www.dpmo.cz/"
        },
        {
            "name": "Železnice Slovenskej republiky",
            "url": "https://www.zsr.sk/"
        },
        {
            "name": "KORDIS JMK, a.s.",
            "url": "https://www.idsjmk.cz/"
        },
    ],
}