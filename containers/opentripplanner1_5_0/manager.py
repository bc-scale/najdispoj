import docker
import logging
import os

from server.core.container import Container
from server.core.gtfsfolder import GTFSFolder

import server.config as config


class OpenTripPlanner1_5_0Manager:
    # TODO Use stuff from config
    # TODO Remove comment?
    """
    This class can be fully rewritten to accommodate the needs
    of your specific Najdispoj instance. The functions should then be called
    from main.py according to your needs. This class should use the functions
    defined in the Container class.
    """

    def __init__(
        self,
        work_dir: str,
        port: int = 25515,
        region: str = "default",
        xmx: str = "1G",
    ):
        logging.info(
            f"Initializing OpenTripPlanner 1.5.0 manager. Region: {region}"
        )

        self.work_dir = work_dir
        self.port = port
        self.region = region
        self.graph_path = work_dir + "/graphs/" + region
        self.xmx = xmx

        # Create graph_path if it doesn't exist
        os.makedirs(self.graph_path, exist_ok=True)

        self.client = docker.from_env()
        self._set_up_container()

    def graph_exists(self):
        return os.path.exists(self.graph_path + "/Graph.obj")

    def link_osm(self, path: str):
        """
        Create a symlink - OTP 1.5.0 can't find the required files in the
        folder structure of najdispoj_data volume. OTP 1.5.0 requires all the
        files to be located in the same folder.
        """
        logging.info(f"🗺️  Linking OSM from {path}...")

        self.container.run_command(f"ln -s {path} {self.graph_path}/merged.pbf")

        logging.info("✅ Done.")
        return self

    def _set_up_container(self):
        logging.info("Setting up OpenTripPlanner 1.5.0 container...")

        self.container = Container(
            self.client,
            os.path.dirname(__file__),
            "opentripplanner150:latest",
            "opentripplanner150",
            "najdispoj_network",  # Declared in docker-compose.yml
            {"25515/tcp": 25515},
            ["najdispoj_data:/mnt/data"],  # Declared in docker-compose.yml
            {"WORK_DIR": self.work_dir},
            restart_on_reload=False,
        )

    def build_graph(self, gtfs_folders: list[GTFSFolder] = []):
        """Runs the build command."""
        logging.info("Building graph...")

        for gtfs_folder in gtfs_folders:
            # Copy zipped GTFS
            gtfs_folder.zip(f"{self.graph_path}/gtfs_{gtfs_folder.label}.zip")

        self.container.run_command(
            f"java -Xmx{self.xmx} -jar otp-1.5.0-shaded.jar --build {self.graph_path}",
            True,
            detach=False,
        )

        return self

    def serve(self, restart=True):
        """Runs the serve command (with the option of a forced restart)."""
        logging.info("Serving data...")

        if restart:
            self.container.restart_container()  # TODO only if is running

        self.container.run_command(
            f"java -Xmx{self.xmx} -jar otp-1.5.0-shaded.jar --server --port {self.port} "
            "--basePath " + self.work_dir + " --router " + self.region,
            log_output=True,
            wait_until="Grizzly server running.",
        )

        return self
